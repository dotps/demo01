using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    [SerializeField] private Vector3 _direction = Vector3.down;
    [SerializeField] private float _speed = 1f;
    [SerializeField] private float _range = 1f;
    [SerializeField] private bool _isMoveWithPlayer;
    [SerializeField] private float _boundRange = 0.01f;

    private Vector3 _startPosition;
    private Vector3 _finishPosition;
    private Vector3 _nextPosition;
    
    void Start()
    {
        _startPosition = transform.position - _range * _direction;
        _finishPosition = transform.position + _range * _direction;
        _nextPosition = _finishPosition;
        transform.localPosition = _startPosition;

        StartCoroutine(Move(_nextPosition));
    }

    private IEnumerator Move(Vector3 finishPosition)
    {
        float time = 0f;
        Vector3 startPosition = transform.position;
        bool isMoving = true;

        while (isMoving)
        {
            transform.position = Vector3.Lerp(startPosition, finishPosition, time * _speed);
            time += Time.deltaTime;
            
            float distance = (transform.position - finishPosition).magnitude;

            if (distance < _boundRange)
            {
                isMoving = false;
            }
            yield return null;
        }

        _nextPosition = finishPosition == _startPosition ? _finishPosition : _startPosition;
        StartCoroutine(Move(_nextPosition));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_isMoveWithPlayer)
        {
            collision.gameObject.transform.parent = transform;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (_isMoveWithPlayer)
        {
            other.gameObject.transform.parent = null;
        }
    }

}
