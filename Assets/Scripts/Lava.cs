using System;
using UnityEngine;

public class Lava : MonoBehaviour
{
  public float DamagePerSecond = 5f;
  private Game _gameManager; 
  
  private void Start()
  {
    _gameManager = FindObjectOfType<Game>();
  }

  private void OnCollisionStay(Collision collisionInfo)
  {
    _gameManager.Player.TakeDamage(DamagePerSecond);
  }
}